package com.zuitt;

import java.util.Scanner;

public class LeapYearApp {

    public static void main(String[] args){
        Scanner appScanner = new Scanner(System.in);

        System.out.println("Input year to be checked if it is a leap year:");
        int checkYear = Integer.valueOf(appScanner.nextLine());

        if(checkYear % 400 == 0 || ((checkYear % 4 == 0) && (checkYear %100 != 0))){
            System.out.println(checkYear + " is a leap year");

        } else {
            System.out.println(checkYear + " is NOT a leap year");
        }
    }
}
